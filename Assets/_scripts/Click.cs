﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class Click : MonoBehaviour {

	public GameObject Enable_Disable;
    public GameObject computer;
    public GameObject ticket_seller;
    //public GameObject collider_position;

	public GameObject button;

    GameObject global_bb_go;
    Blackboard global_bb;

    public void Awake()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();

        if (Enable_Disable.gameObject.name == "Desk1")
        {
            global_bb.SetValue("Desk1Vip_Available", false);
        }
        else if (Enable_Disable.gameObject.name == "Desk2")
        {
            global_bb.SetValue("Desk2_Available", false);
        }
        else if (Enable_Disable.gameObject.name == "Desk4")
        {
            global_bb.SetValue("Desk4_Available", false);
        }
        else if (Enable_Disable.gameObject.name == "Desk5")
        {
            global_bb.SetValue("Desk5_Available", false);
        }
        else if (Enable_Disable.gameObject.name == "SI_Prop_Vending_Machine 4")
        {
            global_bb.SetValue("Machine4_Available", false);
        }
        else if (Enable_Disable.gameObject.name == "SI_Prop_Vending_Machine 5")
        {
            global_bb.SetValue("Machine5_Available", false);
        }
        else if (Enable_Disable.gameObject.name == "SI_Prop_Vending_Machine 6")
        {
            global_bb.SetValue("Machine6_Available", false);
        }
    }
    public void Start()
    {
        Enable_Disable.SetActive(false);
        computer.SetActive(false);
        ticket_seller.SetActive(false);
        
		
    }
    // Use this for initialization
    public void Enable()
    {
        Enable_Disable.SetActive(true);
        computer.SetActive(true);
        ticket_seller.SetActive(true);
        //collider_position.SetActive(true);

        if (Enable_Disable.gameObject.name == "Desk1")
        {
            global_bb.SetValue("Desk1Vip_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "Desk2")
        {
            global_bb.SetValue("Desk2_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "Desk4")
        {
            global_bb.SetValue("Desk4_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "Desk5")
        {
            global_bb.SetValue("Desk5_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "SI_Prop_Vending_Machine 4")
        {
            global_bb.SetValue("Machine4_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "SI_Prop_Vending_Machine 5")
        {
            global_bb.SetValue("Machine5_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "SI_Prop_Vending_Machine 6")
        {
            global_bb.SetValue("Machine6_Available", true);
        }
        else if (Enable_Disable.gameObject.name == "SecurityScannerVip")
        {
            global_bb.SetValue("VipControl_Available", true);
        }
    }

    public void Disable()
    {
        Enable_Disable.SetActive(false);
    }

	public void DisableButton()
	{
		button.SetActive (false);
	}

 
}
