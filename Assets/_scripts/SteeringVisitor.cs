﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SteeringVisitor : MonoBehaviour
{
    private NavMeshAgent navComponent;
    //Move move;

    //desks
    public GameObject[] all_desks = null;
    //public List<GameObject> active_desks = null;

    //QueueLastPos
    /*public static GameObject QueueLastPos;
    public bool qLastPos = true;*/

    //vending machines
   /* public GameObject[] all_machines = null;
    public List<GameObject> active_machines = null;*/

    //benches
    /*public GameObject[] all_benches = null;
    public List<GameObject> active_benches = null;*/

    public float min_distance = 0.8f;
    public float slow_distance = 5.0f;
    public float time_to_target = 0.1f;

    public bool inDesk = false;
    public bool inMachine = false;
    public bool inBench = false;
    public bool inQueue = false;
    public bool inPlane = false;

    public bool desks_empty = true;
    //public bool machines_empty = true;
    //public bool benches_empty = true;

    
    void Start()
    {
        //move = GetComponent<Move>();
        navComponent = GetComponent<NavMeshAgent>();

        //QueueLastPos = GameObject.Find("QueueLastPos");
       // qLastPos = QueueLastPos.GetComponent<Properties>().Available;

        //FindActiveDesks();
       // FindActiveMachines();
       // FindActiveBenches();
    }

    // Update is called once per frame
    void Update()
    {
        //FindActiveDesks();
        //FindActiveMachines();
        //FindActiveBenches();

        //Steer();
    }

    public void FindActiveDesks()
    {
        all_desks = GameObject.FindGameObjectsWithTag("Desk");
        foreach (GameObject desk in all_desks)
        {
            if (desk.GetComponent<Click>().Enable_Disable.activeSelf == true)
            {
                /*if (desk.GetComponent<Properties>().Available == true)
                {
                    active_desks.Add(desk);
                }*/
            }
        }
        desks_empty = false;

    }
    /*
    public void FindActiveMachines()
    {
        all_machines = GameObject.FindGameObjectsWithTag("VendingMachine");
        foreach (GameObject mach in all_machines)
        {
            if (mach.GetComponent<Click>().Enable_Disable.activeSelf == true)
            {
                if (mach.GetComponent<Properties>().Available == true)
                {
                    active_machines.Add(mach);
                }
            }
        }
        machines_empty = false;

    }
    public void FindActiveBenches()
    {
        all_benches = GameObject.FindGameObjectsWithTag("Bench");
        foreach (GameObject bench in all_benches)
        {
            if (bench.GetComponent<Click>().Enable_Disable.activeSelf == true)
            {
                if (bench.GetComponent<Properties>().Available == true)
                {
                    active_benches.Add(bench);
                }
            }
        }
        benches_empty = false;

    }*/



    /*public void Steer(Vector3 target)
    {
        if (!move)
            move = GetComponent<Move>();

        // Velocity we are trying to match
        float ideal_velocity = 0.0f;
        Vector3 diff = target - transform.position;

        if (diff.magnitude < min_distance)
            move.SetMovementVelocity(Vector3.zero, 0);

        // Decide wich would be our ideal velocity
        if (diff.magnitude > slow_distance)
            ideal_velocity = move.max_mov_velocity;
        else
            ideal_velocity = move.max_mov_velocity * diff.magnitude / slow_distance;

        // Create a vector that describes the ideal velocity
        Vector3 ideal_movement = diff.normalized * ideal_velocity;

        // Calculate acceleration needed to match that velocity
        Vector3 acceleration = ideal_movement - move.movement;
        acceleration /= time_to_target;

        // Cap acceleration
        if (acceleration.magnitude > move.max_mov_acceleration)
        {
            acceleration.Normalize();
            acceleration *= move.max_mov_acceleration;
        }

        move.AccelerateMovement(acceleration, 0);
    }*/
}