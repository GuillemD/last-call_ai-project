﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;


public class FindBench : ActionTask<UnityEngine.AI.NavMeshAgent> {

    SteeringVisitor st;
   // Properties pt;
    public GameObject target;
    public float speed = 3f;

    protected override void OnExecute()
    {
        st = agent.GetComponent<SteeringVisitor>();
        agent.speed = speed;

        if (target != null)
            target = null;

        //target = FindTargetBench(pt.active_benches);
        agent.SetDestination(target.transform.position);

        if ((agent.transform.position - target.transform.position).magnitude < agent.stoppingDistance + st.min_distance)
        {
            //EndAction(true);
            return;
        }

        GoToBenchAndSit();
    }
    protected override void OnUpdate()
    {
        GoToBenchAndSit();
    }

    void GoToBenchAndSit()
    {
        var pos = target.transform.position;
        int i = 0;
        //st.Steer(pos);

        if ((target.transform.position - agent.transform.position).magnitude <= 2)
        {
            agent.speed = 0;
            for(i = 0; i<=4; i++)
            {
                if(i == 4)
                {
                    agent.speed = 3;
                }
            }
            EndAction(true);
        }
            
    }


    GameObject FindTargetBench(List<GameObject> list)
    {
        List<GameObject> targetList = list;
        int index;

        if (targetList.Count == 0)
        {
           // EndAction(false);
            return null;
        }

        for (index = 0; index < targetList.Count; index++)
        {
            /*if (targetList[index].GetComponent<Properties>().Available == true)
            {
                return targetList[index];
            }*/
        }

        return targetList[-1];
    }
}
