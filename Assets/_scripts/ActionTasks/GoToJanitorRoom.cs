﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using NodeCanvas.Framework;

public class GoToJanitorRoom :ActionTask<UnityEngine.AI.NavMeshAgent>  {

    SteeringJanitor st;
    GameObject target;

    float speed = 2f;

	protected override void OnExecute() 
	{
		
	    st = agent.GetComponent<SteeringJanitor> ();
        agent.speed = speed;

        target = GameObject.Find("janitor_position");
        agent.SetDestination(target.transform.position);

        agent.speed = speed;
        if ((agent.transform.position - target.transform.position).magnitude < agent.stoppingDistance + st.min_distance)
        {
            // EndAction(true);
            return;
        }
        MovetoRoom();
    }
	

	protected override void OnUpdate(){

		MovetoRoom ();
	}

	public void MovetoRoom()
	{
        st.Steer(target.transform.position);

        float aux = (target.transform.position - agent.transform.position).magnitude;
        if (aux <= 2)
        {
            
            EndAction(true);
        }

    }
}
