﻿using System.Collections;using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using NodeCanvas.Framework;

public class CleanLitter : ActionTask<UnityEngine.AI.NavMeshAgent> {

	SteeringJanitor st;

	public GameObject target;
	public float speed = 3f;

	protected override void OnExecute ()
	{
		st = agent.GetComponent<SteeringJanitor> ();
		if (target != null)
			target = null;

		target = FindTargetLitter (st.active_litters);
        agent.SetDestination(target.transform.position);

		agent.speed = speed;
		if ((agent.transform.position - target.transform.position).magnitude < agent.stoppingDistance + st.min_distance)
		{
			//EndAction (true);
			return;
		}

		MoveToLitter ();
	}

	protected override void OnUpdate()
	{
		MoveToLitter ();
	}


	void MoveToLitter()
	{	

		st.Steer (target.transform.position);

        float aux = (target.transform.position - agent.transform.position).magnitude;
        if (aux <= 1)
        {
            EndAction(true);
        }


    }


	GameObject FindTargetLitter (List<GameObject>list)
	{
		List <GameObject> targetList = list;
		int index;
		if (targetList.Count == 0) 
		{
			//EndAction (false);
			return null;

		}

		for (index = 0; index < targetList.Count; index++) 
		{
			/*if(targetList[index].GetComponent<Properties>().Available == true)
			{
				
					return targetList[index];

			}*/
		}

		return targetList [-1];
	}


}
