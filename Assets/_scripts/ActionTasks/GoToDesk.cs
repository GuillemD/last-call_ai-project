﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class GoToDesk : ActionTask<UnityEngine.AI.NavMeshAgent>
{

    GameObject target;
    GameObject global_bb_go;
    Blackboard global_bb;

    bool desk2_active, desk3_active, desk4_active, desk5_active;

    public BBParameter<float> speed;

    private float arrive_dist = 1.2f;
    //private float waiting_time = 3f;

    private float dist;

    protected override string OnInit()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();

        
        desk2_active = global_bb.GetValue<bool>("Desk2_Available");
        desk3_active = global_bb.GetValue<bool>("Desk3_Available");
        desk4_active = global_bb.GetValue<bool>("Desk4_Available");
        desk5_active = global_bb.GetValue<bool>("Desk5_Available");

        return null;

    }
    protected override void OnExecute()
    {
        
        if (target != null)
            target = null;

        agent.speed = speed.value;
        

        FindTargetDesk();
        agent.SetDestination(target.transform.position);

        MoveToDesk();

    }

    protected override void OnUpdate()
    {
        MoveToDesk();

        
        if (dist <= arrive_dist)
        {

            if(target.name =="Desk_pos2")
            {
                desk2_active = true;
                global_bb.SetValue("Desk2_Available", true);
            }
            else if(target.name =="Desk_pos3")
            {
                desk3_active = true;
                global_bb.SetValue("Desk3_Available", true);
            }
            else if(target.name =="Desk_pos4")
            {
                desk4_active = true;
                global_bb.SetValue("Desk4_Available", true);
            }
            else if(target.name == "Desk_pos5")
            {
                desk5_active = true;
                global_bb.SetValue("Desk5_Available", true);
            }

            EndAction(true);
                     
        }

    }
    

    void MoveToDesk()
    {

        agent.transform.LookAt(target.transform.position);

        dist = (target.transform.position - agent.transform.position).magnitude;

    }

    void FindTargetDesk()
    {
        if (desk2_active)
        {
            global_bb.SetValue("Desk2_Available", false);
            desk2_active = false;
            target = GameObject.Find("Desk_pos2");
        }
        else if (desk3_active)
        {
            global_bb.SetValue("Desk3_Available", false);
            desk3_active = false;
            target = GameObject.Find("Desk_pos3");
        }
        else if (desk4_active)
        {
            global_bb.SetValue("Desk4_Available", false);
            desk4_active = false;
            target = GameObject.Find("Desk_pos4");
        }
        else if (desk5_active)
        {
            global_bb.SetValue("Desk5_Available", false);
            desk5_active = false;
            target = GameObject.Find("Desk_pos5");
        }
        
    }

   

}
    

