﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class GoToMachine : ActionTask<UnityEngine.AI.NavMeshAgent> {

    GameObject target;
    GameObject global_bb_go;
    Blackboard global_bb;

    bool machine1_active, machine2_active, machine3_active, machine4_active, machine5_active, machine6_active;

    public BBParameter<float> speed;

    private float arrive_dist = 1f;
    //private float waiting_time = 3f;

    private float dist;

    protected override string OnInit()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();

        machine1_active = global_bb.GetValue<bool>("Machine1_Available");
        machine2_active = global_bb.GetValue<bool>("Machine2_Available");
        machine3_active = global_bb.GetValue<bool>("Machine3_Available");
        machine4_active = global_bb.GetValue<bool>("Machine4_Available");
        machine5_active = global_bb.GetValue<bool>("Machine5_Available");
        machine6_active = global_bb.GetValue<bool>("Machine5_Available");

        return null;

    }
    protected override void OnExecute()
    {
        if (target != null)
            target = null;

        agent.speed = speed.value;

	
        FindTargetMachine();
        agent.SetDestination(target.transform.position);

        MoveToMachine();
    }

    protected override void OnUpdate()
    {
        MoveToMachine();

        if (dist <= arrive_dist)
        {

            if (target.name == "Machine1_pos")
            {
                machine1_active = true;
                global_bb.SetValue("Machine1_Available", true);
            }
            else if (target.name == "Machine2_pos")
            {
                machine2_active = true;
                global_bb.SetValue("Machine2_Available", true);
            }
            else if (target.name == "Machine3_pos")
            {
                machine3_active = true;
                global_bb.SetValue("Machine3_Available", true);
            }
            else if (target.name == "Machine4_pos")
            {
                machine4_active = true;
                global_bb.SetValue("Machine4_Available", true);
            }
            else if (target.name == "Machine5_pos")
            {
                machine5_active = true;
                global_bb.SetValue("Machine5_Available", true);
            }
            else if (target.name == "Machine6_pos")
            {
                machine6_active = true;
                global_bb.SetValue("Machine6_Available", true);
            }

            EndAction(true);

        }

    }

    void MoveToMachine()
    {
        agent.transform.LookAt(target.transform.position);

        dist = (target.transform.position - agent.transform.position).magnitude;
    }


    void FindTargetMachine()
    {
        if (machine1_active)
        {
            global_bb.SetValue("Machine1_Available", false);
            machine1_active = false;
            target = GameObject.Find("Machine1_pos");
        }
        else if (machine2_active)
        {
            global_bb.SetValue("Machine2_Available", false);
            machine2_active = false;
            target = GameObject.Find("Machine2_pos");
        }
        else if (machine3_active)
        {
            global_bb.SetValue("Machine3_Available", false);
            machine3_active = false;
            target = GameObject.Find("Machine3_pos");
        }
        else if (machine4_active)
        {
            global_bb.SetValue("Machine4_Available", false);
            machine4_active = false;
            target = GameObject.Find("Machine4_pos");
        }
        else if (machine5_active)
        {
            global_bb.SetValue("Machine5_Available", false);
            machine5_active = false;
            target = GameObject.Find("Machine5_pos");
        }
        else if (machine6_active)
        {
            global_bb.SetValue("Machine6_Available", false);
            machine6_active = false;
            target = GameObject.Find("Machine6_pos");
        }
    }
}
