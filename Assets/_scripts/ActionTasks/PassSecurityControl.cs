﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class PassSecurityControl : ActionTask<UnityEngine.AI.NavMeshAgent> {

    GameObject target;
    GameObject global_bb_go;
    Blackboard global_bb;

    bool security1, security2, security3;

    public BBParameter<float> speed;

    private float arrive_dist = 1.4f;
    //private float waiting_time = 3f;

    private float dist;

    protected override string OnInit()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();

        security1 = global_bb.GetValue<bool>("Security1_Available");
        security2 = global_bb.GetValue<bool>("Security2_Available");
        security3 = global_bb.GetValue<bool>("Security3_Available");
       

        return null;

    }
    protected override void OnExecute()
    {
        if (target != null)
            target = null;

        agent.speed = speed.value;

        ChooseControl();
        agent.SetDestination(target.transform.position);
        MoveToControl();
    }
    protected override void OnUpdate()
    {
        MoveToControl();
        if(dist <= arrive_dist)
        {
            if(target.name == "Control1_col")
            {
                security1 = true;
                global_bb.SetValue("Security1_Available", true);
            }
            else if (target.name == "Control2_col")
            {
                security2 = true;
                global_bb.SetValue("Security2_Available", true);
            }
            else if (target.name == "Control3_col")
            {
                security3 = true;
                global_bb.SetValue("Security3_Available", true);
            }
            EndAction(true);
        }
    }

    void ChooseControl()
    {
        int rand = Random.Range(0, 2);

        if(rand == 0)
        {
            global_bb.SetValue("Security1_Available", false);
            security1 = false;
            target = GameObject.Find("Control1_col");
                     
        }
        else if (rand == 1)
        {
            global_bb.SetValue("Security2_Available", false);
            security2 = false;
            target = GameObject.Find("Control2_col");
           
        }
        else if (rand == 2)
        {
            global_bb.SetValue("Security3_Available", false);
            security3 = false;
            target = GameObject.Find("Control3_col");
        }


    }
    void MoveToControl()
    {
        agent.transform.LookAt(target.transform.position);

        dist = (target.transform.position - agent.transform.position).magnitude;

        agent.SetDestination(target.transform.position);
    }
}
