﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class ShowIcon : ActionTask<UnityEngine.AI.NavMeshAgent>
{
    public BBParameter<Sprite> icon_to_show;
    public BBParameter<GameObject> owner;
    private UIfollow ui;
    public BBParameter<float> time;


    protected override string OnInit()
    {
        ui = owner.value.GetComponent<UIfollow>();
        return null;
    }
    protected override void OnExecute()
    {
        ui.ShowImage(icon_to_show.value,time.value);
        EndAction(true);
    }

    protected override void OnUpdate()
    {
       
    }

}
