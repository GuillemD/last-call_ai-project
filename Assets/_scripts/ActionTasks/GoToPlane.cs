﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class GoToPlane : ActionTask<UnityEngine.AI.NavMeshAgent> {

    
    GameObject target;

    public BBParameter<float> speed;

    private float arrive_dist = 1f;
    private float dist;

    protected override void OnExecute()
    {
        if (target != null)
            target = null;
        agent.speed = speed.value;

        target = GameObject.FindGameObjectWithTag("Plane");
        agent.SetDestination(target.transform.position);

        
        MoveToPlane();
    }
    protected override void OnUpdate()
    {
        MoveToPlane();

        if(dist <= arrive_dist)
        {
            EndAction(true);
        }
    }
    void MoveToPlane()
    {
        agent.transform.LookAt(target.transform.position);

        dist = (target.transform.position - agent.transform.position).magnitude;

    }
}
