﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;


public class StandInQueue : ActionTask<UnityEngine.AI.NavMeshAgent> {

    SteeringVisitor st;
    float speed = 3f;
    //GameObject[] queue_points = null;
    GameObject target = null;
    GameObject nextTarget = null;

    protected override void OnExecute()
    {
        st = agent.GetComponent<SteeringVisitor>();
        agent.speed = speed;

        //queue_points = GameObject.FindGameObjectsWithTag("Queue");
        if (target != null)
            target = null;

        target = GameObject.Find("Q8");
        

        FindNextTarget();
        GoToQueuePoint();
        
    }
    protected override void OnUpdate()
    {
        //FindNextTarget();
        GoToQueuePoint();
    }
    void FindNextTarget()
    {
       
        if(target.name == "Q8")
        {
            nextTarget = GameObject.Find("Q7");
        }
        else if (target.name == "Q7")
        {
            nextTarget = GameObject.Find("Q6");
        }
        else if (target.name == "Q6")
        {
            nextTarget = GameObject.Find("Q5");
        }
        else if (target.name == "Q5")
        {
            nextTarget = GameObject.Find("Q4");
        }
        else if (target.name == "Q4")
        {
            nextTarget = GameObject.Find("Q3");
        }
        else if (target.name == "Q3")
        {
            nextTarget = GameObject.Find("Q2");
        }
        else if (target.name == "Q2")
        {
            nextTarget = GameObject.Find("Q1");
        }
    }

    void GoToQueuePoint()
    {
        
        agent.SetDestination(target.transform.position);

        //st.Steer(target.transform.position);

        float aux = (agent.transform.position - target.transform.position).magnitude;
        if (aux <=  1)
        {
            if(target.name == "Q1")
            {
                EndAction(true);
            }
            else
            {
                target = nextTarget;
                FindNextTarget();
            }
                
        }
    }
}
