﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class FindNearestExit : ActionTask<UnityEngine.AI.NavMeshAgent> {

    

    
    public BBParameter<GameObject> target_exit;
    public BBParameter<float> speed;

    protected override void OnExecute()
    {

        if (target_exit != null)
            target_exit = null;

       
        //agent.SetDestination(target_exit.tra);

        agent.speed = speed.value;
        

        GoToExit();
     
    }

    protected override void OnUpdate()
    {
        GoToExit();
    }

   

    void GoToExit()
    {
        //var pos = target.transform.position;

        //st.Steer(pos);

       // if (agent.remainingDistance <= agent.stoppingDistance + st.min_distance)
            EndAction(true);
    }
}
