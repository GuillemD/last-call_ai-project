﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class GoToQueue : ActionTask<UnityEngine.AI.NavMeshAgent> {

   
    GameObject target;
    GameObject nextTarget;
    GameObject global_bb_go;
    Blackboard global_bb;
    
    public BBParameter<float> speed;

    bool queue_last_pos, Q9, Q8, Q7, Q6, Q5, Q4, Q3, Q2, Q1;

    private float arrive_dist = 1f;
    private float dist;
    private float wait = 2f;

    protected override string OnInit()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();

        Q1 = global_bb.GetValue<bool>("Q1_Available");
        Q2 = global_bb.GetValue<bool>("Q2_Available");
        Q3 = global_bb.GetValue<bool>("Q3_Available");
        Q4 = global_bb.GetValue<bool>("Q4_Available");
        Q5 = global_bb.GetValue<bool>("Q5_Available");
        Q6 = global_bb.GetValue<bool>("Q6_Available");
        Q7 = global_bb.GetValue<bool>("Q7_Available");
        Q8 = global_bb.GetValue<bool>("Q8_Available");
        Q9 = global_bb.GetValue<bool>("Q9_Available");
        queue_last_pos = global_bb.GetValue<bool>("Qlastpos_Available");

        return null;

    }
    protected override void OnExecute()
    {

        if (target != null)
            target = null;

        agent.speed = speed.value;

        FindQueueTarget();

        agent.SetDestination(target.transform.position);


        MoveToQueuePos();

    }
    protected override void OnUpdate()
    {
        MoveToQueuePos();

        if(dist <= arrive_dist)
        {
            if(target.name == "Q_1")
            {
                EndAction(true);
            }
            else
            {
                FindQueueTarget();
            }
        }
    }

    void MoveToQueuePos()
    {
        agent.transform.LookAt(target.transform.position);

        dist = (target.transform.position - agent.transform.position).magnitude;

    }

    void FindQueueTarget()
    {
        if(target == null)
        {
            if (queue_last_pos)
            {
                global_bb.SetValue("Qlastpos_Available", false);
                queue_last_pos = false;
                target = GameObject.Find("Q_lastpos");
                nextTarget = GameObject.Find("Q_9");
            }
                            
        }else
        {
            if(target.gameObject.Equals(GameObject.Find("Q_lastpos")))
            {
                if(Q9)
                {
                    global_bb.SetValue("Q9_Available", false); //occupying next spot
                    Q9 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_8");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Qlastpos_Available", true); //freeing previous target
                    queue_last_pos = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_9")))
            {
                if (Q8)
                {
                    global_bb.SetValue("Q8_Available", false);
                    Q8 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_7");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q9_Available", true);
                    Q9 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_8")))
            {
                if (Q7)
                {
                    global_bb.SetValue("Q7_Available", false);
                    Q7 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_6");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q8_Available", true);
                    Q8 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_7")))
            {
                if (Q6)
                {
                    global_bb.SetValue("Q6_Available", false);
                    Q6 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_5");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q7_Available", true);
                    Q7 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_6")))
            {
                if (Q5)
                {
                    global_bb.SetValue("Q5_Available", false);
                    Q5 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_4");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q6_Available", true);
                    Q6 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_5")))
            {
                if (Q4)
                {
                    global_bb.SetValue("Q4_Available", false);
                    Q4 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_3");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q5_Available", true);
                    Q5 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_4")))
            {
                if (Q3)
                {
                    global_bb.SetValue("Q3_Available", false);
                    Q3 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_2");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q4_Available", true);
                    Q4 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_3")))
            {
                if (Q2)
                {
                    global_bb.SetValue("Q2_Available", false);
                    Q2 = false;
                    target = nextTarget;
                    nextTarget = GameObject.Find("Q_1");
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q3_Available", true);
                    Q3 = true;
                }
                else
                {
                    Wait();
                }
            }
            else if (target.gameObject.Equals(GameObject.Find("Q_2")))
            {
                if (Q1)
                {
                    global_bb.SetValue("Q1_Available", false);
                    Q1 = false;
                    target = nextTarget;
                    nextTarget = null;
                    agent.SetDestination(target.transform.position);
                    global_bb.SetValue("Q2_Available", true);
                    Q2 = true;
                }
                else
                {
                    Wait();
                }
            }
        }
    }
    void Wait()
    {
        /*agent.GetComponent<Animator>().Play("Idle");
        wait -= Time.deltaTime;

        if(wait <= 0)
        {
            FindQueueTarget();
        }*/
    }
}
