﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;


public class Litter : ActionTask<UnityEngine.AI.NavMeshAgent> {

    
    Spawner spawn;

    public BBParameter<float> speed;

    public BBParameter<GameObject> litter_prefab;

    int litter_range = 50;

    Vector3 targetPos;
    private float arrive_dist = 1f;
    private float dist;

    protected override void OnExecute()
    {
        spawn = agent.GetComponent<Spawner>();
        agent.speed = speed.value;

        targetPos = CalculateLitterPos();

        

        Littering();
    }
    protected override void OnUpdate()
    {
        Littering();
        if(dist <= arrive_dist)
        {
            spawn.MakeThingToSpawn(litter_prefab.value);
            EndAction();
        }
    }

    void Littering()
    {
        agent.transform.LookAt(targetPos);
        agent.SetDestination(targetPos);

        dist = (targetPos - agent.transform.position).magnitude;

    }

    Vector3 CalculateLitterPos()
    {
        
        Vector3 litterPos = (Random.insideUnitSphere * litter_range) + agent.transform.position;

        if (litterPos.x <= 6.2f || litterPos.x >= 24f)
            litterPos.x = Random.Range(6.2f, 24f);
        if (litterPos.z <= -12f || litterPos.z >= 9f)
            litterPos.z = Random.Range(-12f,9f);
        litterPos.y = 6f;
        

        return litterPos;
    }
}
