﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using NodeCanvas.Framework;

public class RepairMachine : ActionTask<UnityEngine.AI.NavMeshAgent> {

	SteeringJanitor st;

	public GameObject target;
	public float speed = 3f;

	Properties ut;

	protected override void OnExecute()
	{
		st = agent.GetComponent<SteeringJanitor> ();

		ut = agent.GetComponent<Properties> ();

		if (target != null)
			target = null;

		target = FindTargetMachine (st.repair_machines);
        agent.SetDestination(target.transform.position);

		agent.speed = speed;
		if ((agent.transform.position - target.transform.position).magnitude < agent.stoppingDistance + st.min_distance)
		{
			//EndAction (true);
			return;
		}

		MovetoMachine ();

	}

	protected override void OnUpdate()
	{
		MovetoMachine ();
	}

	void MovetoMachine ()
	{
        st.Steer(target.transform.position);

        float aux = (target.transform.position - agent.transform.position).magnitude;
        if (aux <= 2)
        {
			FixMachine();
			EndAction(true);
		}
	}

	void FixMachine()
	{
		
		//target.GetComponent<Properties> ().Available = true;
		//target.GetComponent<Properties> ().Broken = false;
		//ut.broken_counter = 0;
		
	}

	GameObject FindTargetMachine (List<GameObject>list)
	{
		List <GameObject> targetList = list;
		int index;
		if (targetList.Count == 0) 
		{
			//EndAction (false);
			return null;

		}

		for (index = 0; index < targetList.Count; index++) 
		{
			/*if(targetList[index].GetComponent<Properties>().Available == false)
			{
				if (targetList [index].GetComponent<Properties> ().Broken == true) 
				{
					return targetList[index];
				}
			}*/
		}

		return targetList [-1];
	}



}
