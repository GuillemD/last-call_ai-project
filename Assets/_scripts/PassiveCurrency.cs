﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class PassiveCurrency : MonoBehaviour {

    GameObject global_bb_go;
    Blackboard global_bb;

    int money;

    float increase_time = 5f;

    // Use this for initialization
    void Start () {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();


        money = global_bb.GetValue<int>("currency");

        InvokeRepeating("GainMoney", increase_time, increase_time);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void GainMoney()
    {
        money = global_bb.GetValue<int>("currency");
        money += 2;
        global_bb.SetValue("currency", money);
    }
}
