﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class DayNightCycle : MonoBehaviour {

	public  Light sun;
	public  float secondsinFullDay = 120f;

	

	public TimeSpan clocktime;
	public Text timetext;
	public Text timeday;
	public float time;
	public int days;
    public int secondsday = 86400;

	[Range(0,1)] public float currentTimeOfDay = 0;
	private float timeMultiplier = 1f;
	private float sunInitialIntensity;

	public bool night = true;

	void Start()
	{
		sunInitialIntensity = sun.intensity;
		days = 0;
        time = Time.time;
		ChangeDay();
		
	}



	void Update ()
	{
		UpdateSun ();

		currentTimeOfDay += (Time.deltaTime / secondsinFullDay) * timeMultiplier;
       

		ChangeTime ();
		ChangeDay ();



		if (currentTimeOfDay >= 1) {
			currentTimeOfDay = 0;


		}


	}

	void UpdateSun()
	{
		sun.transform.localRotation = Quaternion.Euler ((currentTimeOfDay * 360f) - 90, 170, 0);



		float intensityMultiplier = 1;

		if (currentTimeOfDay <= 0.23f || currentTimeOfDay >= 0.75f) {
			intensityMultiplier = 0;
			night = false;
			
		} else if (currentTimeOfDay <= 0.25f) {
			intensityMultiplier = Mathf.Clamp01 (1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));
		} else if (currentTimeOfDay >= 0.73f) {
			intensityMultiplier = Mathf.Clamp01 (1 - ((currentTimeOfDay - 0.73f) * (1 / 0.02f)));

		} else {
			night = true;
				
		}
			sun.intensity = sunInitialIntensity * intensityMultiplier;
		

	}

	void ChangeTime()
	{

      //float t = (Time.time *720) - time;
      //string minutes = ((int) t / 60).ToString();
      //string seconds = (t % 60).ToString("f0");

        time += Time.deltaTime * 712;

		if (time > secondsday) 
		{
			
			time = 0;
		}


		clocktime = TimeSpan.FromSeconds (time);
		string[] temptime = clocktime.ToString ().Split (":" [0]);
		timetext.text = temptime [0] + ":" + temptime [1];
      //   timetext.text = minutes + ":" + seconds;

	}

	void ChangeDay()
	{
		if (currentTimeOfDay >= 1)
		{
			days = days + 1;
		}
		timeday.text = "Days:" + days.ToString ();

	}



}
