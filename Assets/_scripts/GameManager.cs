﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{

    protected GameManager gm;
    private InitGlobalBB init;
    private GetFinalValues final;

    GameObject global_bb_go;
    Blackboard global_bb;

    public enum gameStates { Menu, Day, Night, GameOver, BeatGame };

    public static gameStates gameState = gameStates.Night;

    [Range(0, 2)] public int currentState;

   // private GameObject[] GO_List;

    //public int score = 0; //total score = money
    public int completedCycles = 0; //Day+Night cycles
    public bool winCondition = false;
    public bool loseCondition = false;
    private int winCycles = 3; //number of completed cycles needed to beat the game
    public float PM = 0.78f;
    public float AM = 0.18f;

    public float Daytime;

    //Happiness bar fields
    public GameObject happybar;
    public Text happytext;
	//public Text happytext2;
	//public Text nvcctext;
	//public Text viptext;
    public Color32 happyColor;
    public Color32 sadColor;
    private Image barImage;
    private RectTransform happybarTransform;

    private float currHappiness;
	private int nvcc;
	private int nvipcc;



    private float maxHappiness = 100f;
    private float minHapiness = 0f;

    //day and night icons

    public GameObject sun_image;
    public GameObject moon_image;

	//public GameObject lose_scene;
	//public GameObject win_scene;
	public GameObject sound1;
	public GameObject sound2;
	public GameObject sound3;
	//public GameObject sound4;

	//public GameObject panel_blue;
	public GameObject money;
	public GameObject bar;


	public GameObject lastcall;
	public GameObject lastcall_timer;


   
    // Use this for initialization
    private void Awake()
    {
        gm = gameObject.GetComponent<GameManager>();
    }
    void Start()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();
        init = global_bb_go.GetComponent<InitGlobalBB>();
        final = gm.GetComponent<GetFinalValues>();
        

        if (gm == null)
            gm = gameObject.GetComponent<GameManager>();

        gameState = gameStates.Night;
        //sun and moon
        sun_image.SetActive(false);
        moon_image.SetActive(false);

        currentState = 1;

        //bar
        happybarTransform = happybar.GetComponent<RectTransform>();
        barImage = happybar.GetComponent<Image>();

        //last call button
        lastcall.SetActive(true);
        lastcall_timer.SetActive(true);

		//panel_blue.SetActive (false);
		money.SetActive (true);
		bar.SetActive (true);

        UpdateHappinessBar();
		printHighscore ();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHappinessBar();
        Win_Condition();
        Lose_Condition();

		printHighscore ();

        switch (gameState)
        {
            
			
            case gameStates.Day:
                {
				    sound1.SetActive (true);
				    sound2.SetActive (true);
				    sound3.SetActive (false);
				
				    money.SetActive (true);
				    bar.SetActive(true);
				    
				
                    GameObject sun = GameObject.Find("Sun");
                    DayNightCycle c_time = sun.GetComponent<DayNightCycle>();
                    Daytime = c_time.currentTimeOfDay;
                    moon_image.SetActive(false);
                    sun_image.SetActive(true);
                    if (Daytime > PM )
                    {
                        gameState = gameStates.Night;
                        global_bb.SetValue("Night", true);
                    
                        currentState = 1;
                        lastcall.SetActive(false);
                        if(lastcall_timer.gameObject.activeSelf == false)
                        {
                            lastcall_timer.SetActive(true);
                        }
                    

                    }
                    break;
                }
            case gameStates.Night:
                {
				    sound1.SetActive (true);
				    sound2.SetActive (true);
				    sound3.SetActive (false);
				    
				    
				    money.SetActive(true);
				    bar.SetActive(true);
				
                    GameObject sun = GameObject.Find("Sun");
                    DayNightCycle c_time = sun.GetComponent<DayNightCycle>();
                    Daytime = c_time.currentTimeOfDay;
                    moon_image.SetActive(true);
                    sun_image.SetActive(false);
                    if (Daytime > AM && Daytime < PM )
                    {
                                    
                        gameState = gameStates.Day;
                        global_bb.SetValue("Night", false);
                    
                        currentState = 0;
                        completedCycles++;
				        global_bb.SetValue ("Days", completedCycles - 1);

                        if(lastcall.gameObject.activeSelf == false)
                        {
                            lastcall.SetActive(true);
                        }

                        if(lastcall_timer.gameObject.activeSelf == true)
                        {
                            lastcall_timer.SetActive(false);
                        }
                
                    }
                
                    break;
                }

            case gameStates.BeatGame:
                {
                    if (winCondition == true)
                    {
					    sound1.SetActive (false);
					    sound2.SetActive (false);
					    sound3.SetActive (true);
					    money.SetActive (false);
					    bar.SetActive(false);
					    money.SetActive(false);
					    bar.SetActive(false);
					    final.ResetValues();
					    final.GetValues(init.GetDays(), init.GetCycles(0), init.GetCycles(1), init.GetCycles(2), init.GetCycles(3));
                        ResetGame();
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                    }
                    break;
                }
            case gameStates.GameOver:
                {
                    if (loseCondition == true)
                    {
					    sound1.SetActive (false);
					    sound2.SetActive (false);
					    money.SetActive(false);
					    bar.SetActive(false);
					
					    final.ResetValues();
                        final.GetValues(init.GetDays(), init.GetCycles(0), init.GetCycles(1), init.GetCycles(2), init.GetCycles(3));
                        ResetGame();
                        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
                    }

                    break;
                }

        }
    }
    void UpdateHappinessBar()
    {
        currHappiness = global_bb.GetValue<float>("Happiness");
        float ratio = currHappiness / maxHappiness;

        if(currHappiness > 0)
        {
            barImage.color = Color.Lerp(sadColor, happyColor, ratio);
            happytext.text = currHappiness.ToString() + " / " + maxHappiness.ToString();
            happybarTransform.localScale = new Vector3(ratio, 1, 1);
        }
        else
        {
            happytext.text = currHappiness.ToString() + " / " + maxHappiness.ToString();
			//happytext2.text = " 0 " ;
            happybarTransform.localScale = new Vector3(0, 1, 1);
        }
        if (currHappiness > 100)
        {
            barImage.color = Color.Lerp(sadColor, happyColor, ratio);
            happytext.text = maxHappiness.ToString() + " / " + maxHappiness.ToString();
           // happytext2.text =  " 100 " ;
            happybarTransform.localScale = new Vector3(1, 1, 1);
        }
        
    }


    void Win_Condition()
    {
        currHappiness = global_bb.GetValue<float>("Happiness");

        if (completedCycles >= winCycles)
        {
            if (currHappiness >= maxHappiness)
            {
                winCondition = true;
                gameState = gameStates.BeatGame;
            }
        }
    }

   void Lose_Condition()
    {
        currHappiness = global_bb.GetValue<float>("Happiness");

        if(currHappiness <= minHapiness)
        {
            loseCondition = true;
            gameState = gameStates.GameOver;
        }

    }

    public void ResetGame()
    {
        init.ResetBB();
    }

	void printHighscore()
	{
		nvcc = global_bb.GetValue<int>("NormalVisitorsCycleCompleted");
		nvipcc = global_bb.GetValue<int>("VipCyclesCompleted");
        
	}
}


