﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIfollow : MonoBehaviour {


    //private Sprite curr_sprite;

    private Canvas this_canvas; 

    private Image image;

    private bool show_image = false;
    public float show_time = 3f;
    float show_count;

	// Use this for initialization
	void Start ()
    {
        show_count = 0;

        this_canvas = GetComponentInChildren<Canvas>();

        image = this_canvas.GetComponentInChildren<Image>();

        this_canvas.gameObject.SetActive(false);

        
       
	}

	void Update () {

       
        if(show_image)
        {
            show_count += Time.deltaTime;
            if(show_count > show_time && show_image == true)
            {
                show_image = false;
                image.sprite = null;
                this_canvas.gameObject.SetActive(false);
                show_count = 0;
            }
        }
		
	}

    public void ShowImage(Sprite spr, float time)
    {
        show_count = 0;
        show_time = time;
        show_image = true;
        image.sprite = spr;
        this_canvas.gameObject.SetActive(true);
    }


}
