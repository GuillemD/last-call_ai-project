﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;



public class Currency : MonoBehaviour {

    public Text money;
    int currency;

    GameObject global_bb_go;
    Blackboard global_bb;

    public GameObject button1, button2, button3, button4, button5, button6, button7, button8, button9;
	public bool fake_button1;

	static int janitor_cost = 50;
	static int desk1_cost = 100;
    static int desk2_cost = 140;
    static int desk3_cost = 200;
    static int desk_vip = 300;
	static int security_cost = 250;
	static int machine1_cost = 70;
    static int machine2_cost = 90;
    static int machine3_cost = 120;


    private void Awake()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();
    }

    // Use this for initialization
    void Start () {
        currency = global_bb.GetValue<int>("currency");
		button1.SetActive (true);
		button2.SetActive (true);
		button3.SetActive (true);
		button4.SetActive (true);
		button5.SetActive (true);
		button6.SetActive (true);
		button7.SetActive (true);
		button8.SetActive (true);
		button9.SetActive (true);
		Buy ();
    }


     void Update()
    {
		ChangeMoney();
		Buy();
        
    }

     public void BuyDesk1 () {

        currency = currency - desk1_cost;
        global_bb.SetValue("currency", currency);
       // Destroy(button1);

    }
    public void BuyDesk2()
    {

        currency = currency - desk2_cost;
        global_bb.SetValue("currency", currency);
        
       // Destroy(button2);
    }
    public void BuyDesk3()
    {

        currency = currency - desk3_cost;
        global_bb.SetValue("currency", currency);
        
       // Destroy(button3);
    }

    public void BuyVipDesk(){
		
		currency = currency - desk_vip;
        global_bb.SetValue("currency", currency);
        
       // Destroy(button4);
    }

    public void BuySecurity()
    {
        currency = currency - security_cost;
        global_bb.SetValue("currency", currency);
        
       // Destroy(button5);
    }

    public void BuyMachines1()
    {
        currency = currency - machine1_cost;
        global_bb.SetValue("currency", currency);
       
       // Destroy(button6);
    }
    public void BuyMachines2()
    {
        currency = currency - machine2_cost;
        global_bb.SetValue("currency", currency);
        
       // Destroy(button7);
    }
    public void BuyMachines3()
    {
        currency = currency - machine3_cost;
        global_bb.SetValue("currency", currency);
       
       // Destroy(button8);
    }

	public void BuyJanitor()
	{
		currency = currency - janitor_cost;
		global_bb.SetValue ("currency", currency);
	}

    void ChangeMoney()
    {
        currency = global_bb.GetValue<int>("currency");
        money.text =  currency.ToString();
    }

    void Buy()
    {
		if (currency >= 0) {

			if (currency >= desk_vip) {

				button1.SetActive(false);
				button2.SetActive(false);
				button3.SetActive(false);
				button4.SetActive (false);
                button5.SetActive(false);
                button6.SetActive(false);
                button7.SetActive(false);
                button8.SetActive(false);
				button9.SetActive(false);
            }

			if (currency >= security_cost)
            {

				button1.SetActive(false);
                button2.SetActive(false);
				button3.SetActive(false);

				button5.SetActive(false);
				button6.SetActive(false);
				button7.SetActive(false);
                button8.SetActive(false);
               	button9.SetActive(false);
                

            }

			if (currency >= desk3_cost) {

				button1.SetActive(false);
				button2.SetActive(false);
				button3.SetActive(false);

                
				button6.SetActive(false);
				button7.SetActive(false);
                button8.SetActive(false);
				button9.SetActive(false);
               

            }

            if (currency >= desk2_cost)
            {

                button2.SetActive(false);
                button8.SetActive(false);
                button1.SetActive(false);
                button6.SetActive(false);
                button7.SetActive(false);
				button9.SetActive(false);
            }
            if (currency >= machine3_cost)
            {
                button8.SetActive(false);
                button1.SetActive(false);
                button6.SetActive(false);
                button7.SetActive(false);
				button9.SetActive(false);
            }
            if (currency >= desk1_cost)
            {
                button1.SetActive(false);
                button6.SetActive(false);
                button7.SetActive(false);
				button9.SetActive(false);
            }
            if (currency >= machine2_cost)
            {

                button6.SetActive(false);
                button7.SetActive(false);
				button9.SetActive(false);
            }
            if (currency >= machine1_cost)
            {

                button6.SetActive(false);
				button9.SetActive(false);
            }
			if (currency >= janitor_cost)
			{
				button9.SetActive(false);
			}

           
			// $$ //

			if (currency < janitor_cost) {
				button9.SetActive(true);
			}

			 if (currency < machine1_cost) {
				button6.SetActive (true);

			}
			if (currency < machine2_cost) {
				button7.SetActive (true);

			}
			if (currency < machine3_cost) {
				button8.SetActive (true);

			}

			 if (currency < desk1_cost) {
				button1.SetActive (true);

			}
			if (currency < desk2_cost) {
				button2.SetActive (true);

			}
			if (currency < desk3_cost) {
				button3.SetActive (true);

			}
			 if (currency < security_cost) {
				button5.SetActive (true);
			}

			 if (currency < desk_vip) {
				button4.SetActive (true);

			} 


		}



        
    }

    
}
