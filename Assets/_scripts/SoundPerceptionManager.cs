﻿using UnityEngine;
using System.Collections;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class SoundPerceptionManager : MonoBehaviour {

	public bool lastcall_heard = false;
    Blackboard bb;

    float speed;
    float increased_speed;

    //image showing

    public Sprite icon;
    private UIfollow ui;
    private float time = 3f;


    void Start()
    {
        bb = GetComponent<Blackboard>();
        speed = bb.GetValue<float>("Speed");
        increased_speed = speed + 3;

        ui = GetComponentInParent<UIfollow>();
       
    }

    void Update()
    {
        if(lastcall_heard)
        {
            bb.SetValue("Speed", increased_speed);
            bb.SetValue("SoundHeard", true);
            ui.ShowImage(icon, time);            

        }
        else
        {
            bb.SetValue("Speed", speed);
            bb.SetValue("SoundHeard", false);            

        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("AudioEmitter"))
        {
            lastcall_heard = true;
            

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("AudioEmitter"))
        {
            lastcall_heard = false;
            

        }
    }




    // Update is called once per frame
    //   void PerceptionEvent (PerceptionEvent ev) {

    //	if(ev.type == global::PerceptionEvent.types.NEW)
    //	{
    //		Debug.Log("Heard somethinng");
    //		lastcall_heard = true;
    //	}
    //	else
    //	{
    //		Debug.Log("Nothing heard");
    //           lastcall_heard = false;
    //       }
    //}
}
