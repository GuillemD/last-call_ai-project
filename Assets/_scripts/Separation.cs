﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Separation : MonoBehaviour
{

    public LayerMask mask;
    public float search_radius = 1.0f;
    public AnimationCurve falloff;

    NavMeshAgent agent;
    

    // Use this for initialization
    void Start()
    {
        
        agent = GetComponentInParent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(agent.transform.position, search_radius, mask);
        Vector3 final = Vector3.zero;

        foreach (Collider col in colliders)
        {
            GameObject go = col.gameObject;

            if (go == agent.gameObject)
                continue;
            if(go.CompareTag("Player"))
            {
                Vector3 diff = agent.transform.position - go.transform.position;
                float distance = diff.magnitude;
                float acceleration = (1.0f - falloff.Evaluate(distance / search_radius)) * agent.acceleration;

                final += diff.normalized * agent.acceleration / 5;
            }
            
        }

        float final_strength = final.magnitude;
        if (final_strength > 0.0f)
        {
            if (final_strength > agent.acceleration)
                final = final.normalized * agent.acceleration;
            agent.velocity = final;
        }
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, search_radius);
    }
}
