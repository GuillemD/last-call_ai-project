﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Display : MonoBehaviour {

    public string MyString;
    public Text text;
    public float fadeTime;
    public bool displayInfo;

	// Use this for initialization
	void Start () {

        text  = GameObject.Find("LastCall text").GetComponent<Text>();
        text.color = Color.clear;

	}
	
	// Update is called once per frame
	void Update () {

        FadeText();

       
	}

     void OnMouseOver()
    {
        displayInfo = true;
    }

     void OnMouseExit()
    {
        displayInfo = false;
    }

    public void FadeText()
    {

        if (displayInfo)
        {
            text.text = MyString;
            text.color = Color.Lerp(text.color, Color.white, fadeTime * Time.deltaTime);
        }

        else
        {
            text.color = Color.Lerp(text.color, Color.clear, fadeTime * Time.deltaTime);
        }

    }
}
