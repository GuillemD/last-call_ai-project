﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class InitGlobalBB : MonoBehaviour {

    Blackboard global_bb;

    bool night;
    bool deskVipAvailable, desk2Available, desk3Available, desk4Available, desk5Available;
    bool q1, q2, q3, q4, q5, q6, q7, q8, q9, qlast;
    bool controlvip;
    bool machine1, machine2, machine3, machine4, machine5, machine6;
    
    int uses1, uses2, uses3, uses4, uses5, uses6;
    int currency;
    float happy_score;
    int NVCycleCompleted, NVCycleInterrupted;
    int VIPCycleCompleted, VIPCycleInterrupted;

	int Days;

	int ValueDay, ValueNV, ValueVip;

    private void Awake()
    {
        global_bb = GetComponent<GlobalBlackboard>();
        night = true;
        deskVipAvailable = false;
        desk2Available = false;
        desk3Available = true;
        desk4Available = false;
        desk5Available = false;
        q1 = true; q2 = true; q3 = true; q4 = true; q5 = true; q6 = true; q7 = true; q8 = true; q9 = true; qlast = true;
        controlvip = false;
        machine1 = true;
        machine2 = true;
        machine3 = true;
        machine4 = false;
        machine5 = false;
        machine6 = false;
		Days = 0;
        uses1 = uses2 = uses3 = uses4 = uses5 = uses6 = 0;
        currency = 50;
        happy_score = 30f;
        NVCycleCompleted = NVCycleInterrupted = VIPCycleCompleted = VIPCycleInterrupted = 0;
        //night
        global_bb.SetValue("Night", night);
        //currency
        global_bb.SetValue("currency", currency);
        //happiness
        global_bb.SetValue("Happiness", happy_score);
        //Cycles
        global_bb.SetValue("NormalVisitorsCycleCompleted", NVCycleCompleted);
        global_bb.SetValue("NormalVisitorsCycleInterrupted", NVCycleInterrupted);
        global_bb.SetValue("VipCyclesCompleted", VIPCycleCompleted);
        global_bb.SetValue("VipCycleInterrupted", VIPCycleInterrupted);
        //machine counters
        global_bb.SetValue("Machine1Uses", uses1);
        global_bb.SetValue("Machine2Uses", uses2);
        global_bb.SetValue("Machine3Uses", uses3);
        global_bb.SetValue("Machine4Uses", uses4);
        global_bb.SetValue("Machine5Uses", uses5);
        global_bb.SetValue("Machine6Uses", uses6);
        //desks
        global_bb.SetValue("Desk1Vip_Available", deskVipAvailable);
        global_bb.SetValue("Desk2_Available", desk2Available);
        global_bb.SetValue("Desk3_Available", desk3Available);
        global_bb.SetValue("Desk4_Available", desk4Available);
        global_bb.SetValue("Desk5_Available", desk5Available);
        //queue
        global_bb.SetValue("Q1_Available", q1);
        global_bb.SetValue("Q2_Available", q2);
        global_bb.SetValue("Q3_Available", q3);
        global_bb.SetValue("Q4_Available", q4);
        global_bb.SetValue("Q5_Available", q5);
        global_bb.SetValue("Q6_Available", q6);
        global_bb.SetValue("Q7_Available", q7);
        global_bb.SetValue("Q8_Available", q8);
        global_bb.SetValue("Q9_Available", q9);
        global_bb.SetValue("Qlastpos_Available", qlast);
        //controlvip
        global_bb.SetValue("VipControl_Available", controlvip);
        //machines
        global_bb.SetValue("Machine1_Available", machine1);
        global_bb.SetValue("Machine2_Available", machine2);
        global_bb.SetValue("Machine3_Available", machine3);
        global_bb.SetValue("Machine4_Available", machine4);
        global_bb.SetValue("Machine5_Available", machine5);
        global_bb.SetValue("Machine6_Available", machine6);
		global_bb.SetValue("Days",Days);
	
    }
    // Use this for initialization
    void Start () {

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ResetBB()
    {
        night = true;
        deskVipAvailable = false;
        desk2Available = false;
        desk3Available = true;
        desk4Available = false;
        desk5Available = false;
        q1 = true; q2 = true; q3 = true; q4 = true; q5 = true; q6 = true; q7 = true; q8 = true; q9 = true; qlast = true;
        controlvip = false;
        machine1 = true;
        machine2 = true;
        machine3 = true;
        machine4 = false;
        machine5 = false;
        machine6 = false;
		Days = 0;
        uses1 = uses2 = uses3 = uses4 = uses5 = uses6 = 0;
        currency = 50;
        happy_score = 30f;
        NVCycleCompleted = NVCycleInterrupted = VIPCycleCompleted = VIPCycleInterrupted = 0;
        //night
        global_bb.SetValue("Night", night);
        //currency
        global_bb.SetValue("currency", currency);
        //happiness
        global_bb.SetValue("Happiness", happy_score);
        //Cycles
        global_bb.SetValue("NormalVisitorsCycleCompleted", NVCycleCompleted);
        global_bb.SetValue("NormalVisitorsCycleInterrupted", NVCycleInterrupted);
        global_bb.SetValue("VipCyclesCompleted", VIPCycleCompleted);
        global_bb.SetValue("VipCycleInterrupted", VIPCycleInterrupted);
        //machine counters
        global_bb.SetValue("Machine1Uses", uses1);
        global_bb.SetValue("Machine2Uses", uses2);
        global_bb.SetValue("Machine3Uses", uses3);
        global_bb.SetValue("Machine4Uses", uses4);
        global_bb.SetValue("Machine5Uses", uses5);
        global_bb.SetValue("Machine6Uses", uses6);
        //desks
        global_bb.SetValue("Desk1Vip_Available", deskVipAvailable);
        global_bb.SetValue("Desk2_Available", desk2Available);
        global_bb.SetValue("Desk3_Available", desk3Available);
        global_bb.SetValue("Desk4_Available", desk4Available);
        global_bb.SetValue("Desk5_Available", desk5Available);
        //queue
        global_bb.SetValue("Q1_Available", q1);
        global_bb.SetValue("Q2_Available", q2);
        global_bb.SetValue("Q3_Available", q3);
        global_bb.SetValue("Q4_Available", q4);
        global_bb.SetValue("Q5_Available", q5);
        global_bb.SetValue("Q6_Available", q6);
        global_bb.SetValue("Q7_Available", q7);
        global_bb.SetValue("Q8_Available", q8);
        global_bb.SetValue("Q9_Available", q9);
        global_bb.SetValue("Qlastpos_Available", qlast);
        //controlvip
        global_bb.SetValue("VipControl_Available", controlvip);
        //machines
        global_bb.SetValue("Machine1_Available", machine1);
        global_bb.SetValue("Machine2_Available", machine2);
        global_bb.SetValue("Machine3_Available", machine3);
        global_bb.SetValue("Machine4_Available", machine4);
        global_bb.SetValue("Machine5_Available", machine5);
        global_bb.SetValue("Machine6_Available", machine6);
		global_bb.SetValue("Days",Days);
	
    }
    public int GetCycles(int i)
    {
        if(i == 0)
        {
			NVCycleCompleted = global_bb.GetValue<int>("NormalVisitorsCycleCompleted");
            return NVCycleCompleted;
        }else if(i ==1)
        {
			NVCycleInterrupted = global_bb.GetValue<int>("NormalVisitorsCycleInterrupted");
            return NVCycleInterrupted;
        }
        else if(i == 2)
        {
            VIPCycleCompleted = global_bb.GetValue<int>("VipCyclesCompleted");
            return VIPCycleCompleted;

        }else if( i == 3)
        {
            VIPCycleInterrupted = global_bb.GetValue<int>("VipCycleInterrupted");
            return VIPCycleInterrupted;
        }


        return 0;

    }
    public int GetDays()
    {
        Days = global_bb.GetValue<int>("Days");

		return Days;
    }



}
