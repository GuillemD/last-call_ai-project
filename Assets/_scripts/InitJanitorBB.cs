﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class InitJanitorBB : MonoBehaviour {

    Blackboard bb;
    List<Transform> control_list = new List<Transform>();
    List<Transform> litter_list = new List<Transform>();
    // Use this for initialization
    void Start()
    {
        bb = GetComponent<Blackboard>();
        bb.SetValue("JanitorRoomPos", GameObject.Find("janitor_position"));
    
        bb.SetValue("Machine1", GameObject.Find("Machine1_pos"));
        bb.SetValue("Machine2", GameObject.Find("Machine2_pos"));
        bb.SetValue("Machine3", GameObject.Find("Machine3_pos"));
        bb.SetValue("Machine4", GameObject.Find("Machine4_pos"));
        bb.SetValue("Machine5", GameObject.Find("Machine5_pos"));
        bb.SetValue("Machine6", GameObject.Find("Machine6_pos"));

        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
