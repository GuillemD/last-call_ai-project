﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;


//public class PerceptionEvent
//{
//	public enum senses { NULL, SOUND };
//	public enum types { NEW, LOST };

//	public GameObject go;
//	public senses sense;
//	public types type;
//}



[RequireComponent(typeof(AudioSource))]
public class SoundEmitter : MonoBehaviour {

    public float range = 20f;
    //public LayerMask mask;
    GameObject go;
    SphereCollider sp;
	//private List<GameObject> agentswhoheardthesound;

	GameObject button;


    public AudioClip lastCall;
    AudioSource audioSource;

    //float time_to_play = 15f;
    //float time_count;

    float time_playing = 7f;
    float wait_time;

    // Use this for initialization
    void Start () {
		//agentswhoheardthesound = new List<GameObject>();

        audioSource = GetComponent<AudioSource>();
        go = GameObject.Find("LastCallEmitter");

        wait_time = 0;

    }
	
	// Update is called once per frame
	void Update () {

        
        wait_time += Time.deltaTime;
        if(wait_time >= time_playing)
        {
            go.SetActive(false);
            wait_time = 0;
        }
      


        
    }

    void PlaySound()
    {
        audioSource.PlayOneShot(lastCall, 0.85f);
    }

    void StopSounds()
    {
        audioSource.enabled = false;
        audioSource.enabled = true;
    }

    public void DrawGizmo()
    {
        Gizmos.color = Color.yellow;
        
        Gizmos.DrawWireSphere(transform.position, range);
    }


}
