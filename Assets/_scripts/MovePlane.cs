﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovePlane : MonoBehaviour
{

    // put the points from unity interface
    public GameObject[] wayPointList;

    public int currentWayPoint = 0;
    GameObject targetWayPoint;

    public float speed = 7f;

    // Use this for initialization
    void Start()
    {
        wayPointList = GameObject.FindGameObjectsWithTag("PlanePoint");
        wayPointList[0] = GameObject.Find("plane1");
        wayPointList[1] = GameObject.Find("plane2");
        wayPointList[2] = GameObject.Find("plane3");
        wayPointList[3] = GameObject.Find("plane4");
        wayPointList[4] = GameObject.Find("plane5");
        wayPointList[5] = GameObject.Find("plane6");
    }

    // Update is called once per frame
    void Update()
    {
        // check if we have somewere to walk
        if (currentWayPoint < this.wayPointList.Length)
        {
            if (targetWayPoint == null)
                targetWayPoint = wayPointList[currentWayPoint];
            walk();
        }
    }

    void walk()
    {
        float aux = (transform.position - targetWayPoint.transform.position).magnitude;
        // rotate towards the target
        transform.forward = Vector3.RotateTowards(transform.forward, targetWayPoint.transform.position - transform.position, speed * Time.deltaTime, 0.0f);

        // move towards the target
        transform.position = Vector3.MoveTowards(transform.position, targetWayPoint.transform.position, speed * Time.deltaTime);

        if (aux <= 1)
        {
            if (currentWayPoint == 5)
            {
                currentWayPoint = 0;
                targetWayPoint = wayPointList[currentWayPoint];
            }
            else
            {
                currentWayPoint++;
                targetWayPoint = wayPointList[currentWayPoint];
            }
        }
    }
}