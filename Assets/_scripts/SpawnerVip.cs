﻿using System.Collections;
using UnityEngine;

public class SpawnerVip : MonoBehaviour {

    public GameObject spawnPrefab;

    private DayNightCycle dn;

    public float minSecondsBetweenSpawning = 40f;
    public float maxSecondsBetweenSpawning = 60f;

    private float savedTime;
    private float secondsBetweenSpawning;

    // Use this for initialization
    void Start () {
        savedTime = Time.time;
        dn = GetComponent<DayNightCycle>();
        secondsBetweenSpawning = Random.Range(minSecondsBetweenSpawning, maxSecondsBetweenSpawning);

    }
	
	// Update is called once per frame
	void Update () {

		if(GameManager.gameState == GameManager.gameStates.Day)
		{

            if (dn.days == 0)
            {
                minSecondsBetweenSpawning = 40f;
                maxSecondsBetweenSpawning = 60f;
            }
            if (dn.days == 1)
            {
                minSecondsBetweenSpawning = 30f;
                maxSecondsBetweenSpawning = 45f;
            }
            if (dn.days >= 2)
            {
                minSecondsBetweenSpawning = 15f;
                maxSecondsBetweenSpawning = 20;
            }
            if (Time.time - savedTime >= secondsBetweenSpawning) // is it time to spawn again?
            {
                MakeThingToSpawn(spawnPrefab);
                savedTime = Time.time; // store for next spawn
                secondsBetweenSpawning = Random.Range(minSecondsBetweenSpawning, maxSecondsBetweenSpawning);
            }

        }
		if(GameManager.gameState == GameManager.gameStates.Night){
            
         }


			
    }

    public void MakeThingToSpawn(GameObject sp_prefab)
    {
        // create a new gameObject
        GameObject clone = Instantiate(sp_prefab, transform.position, transform.rotation) as GameObject;
    }
}
