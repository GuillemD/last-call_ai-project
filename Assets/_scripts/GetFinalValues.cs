﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class GetFinalValues : MonoBehaviour {

	private GameObject manager;

    public float d;
    public int nvcc, vipcc, nvci;

	GameObject global_bb_go;
	Blackboard global_bb;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void GetValues(float days, int normalcomp, int normalint, int vipcomp, int vipint)
    {
        d = days;
        nvcc = normalcomp;
        nvci = normalint;
        vipcc = vipcomp;
        //vipci = vipint;
    }

	public void ResetValues()
	{
		d = 0;
		nvcc = 0;
		nvci = 0;
		vipcc = 0;
	}
}
