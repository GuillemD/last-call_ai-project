﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class UIStatic : MonoBehaviour {

    public GameObject owner;
    GameObject global_bb_go;
    Blackboard global_bb;

    int uses1, uses2, uses3, uses4, uses5, uses6;
    int maxUses = 7;

    private Canvas this_canvas; 

    private Image image;

    private bool show_image;


    private void Awake()
    {
        global_bb_go = GameObject.Find("@GlobalBlackboard");
        global_bb = global_bb_go.GetComponent<GlobalBlackboard>();


        uses1 = global_bb.GetValue<int>("Machine1Uses");
        uses2 = global_bb.GetValue<int>("Machine2Uses");
        uses3 = global_bb.GetValue<int>("Machine3Uses");
        uses4 = global_bb.GetValue<int>("Machine4Uses");
        uses5 = global_bb.GetValue<int>("Machine5Uses");
        uses6 = global_bb.GetValue<int>("Machine6Uses");
    }
    // Use this for initialization
    void Start ()
    {
        show_image = false;

        this_canvas = owner.GetComponentInChildren<Canvas>();

        image = this_canvas.GetComponentInChildren<Image>();

        this_canvas.gameObject.SetActive(false);

	}

	void Update () {

        UpdateUses();

        if(uses1 == maxUses)
        {
            ShowImage();
        }
        else
        {
            if(owner.name == "Machine1_pos")
            {
                show_image = false;
                this_canvas.gameObject.SetActive(false);
            }
            
        }
        if (uses2 == maxUses)
        {
            ShowImage();
        }
        else
        {
            if (owner.name == "Machine2_pos")
            {
                show_image = false;
                this_canvas.gameObject.SetActive(false);
            }
        }
        if (uses3 == maxUses)
        {
            ShowImage();
        }
        else
        {
            if (owner.name == "Machine3_pos")
            {
                show_image = false;
                this_canvas.gameObject.SetActive(false);
            }
        }
        if (uses4 == maxUses)
        {
            ShowImage();
        }
        else
        {
            if (owner.name == "Machine4_pos")
            {
                show_image = false;
                this_canvas.gameObject.SetActive(false);
            }
        }
        if (uses5 == maxUses)
        {
            ShowImage();
        }
        else
        {
            if (owner.name == "Machine5_pos")
            {
                show_image = false;
                this_canvas.gameObject.SetActive(false);
            }
        }
        if (uses6 == maxUses)
        {
            ShowImage();
        }
        else
        {
            if (owner.name == "Machine6_pos")
            {
                show_image = false;
                this_canvas.gameObject.SetActive(false);
            }
        }


    }

    void ShowImage()
    {
        if(owner.name == "Machine1_pos")
        {
            show_image = true;
            this_canvas.gameObject.SetActive(true);
        }
        else if(owner.name == "Machine2_pos")
        {
            show_image = true;
            this_canvas.gameObject.SetActive(true);
        }
        else if (owner.name == "Machine3_pos")
        {
            show_image = true;
            this_canvas.gameObject.SetActive(true);
        }
        else if (owner.name == "Machine4_pos")
        {
            show_image = true;
            this_canvas.gameObject.SetActive(true);
        }
        else if (owner.name == "Machine5_pos")
        {
            show_image = true;
            this_canvas.gameObject.SetActive(true);
        }
        else if (owner.name == "Machine6_pos")
        {
            show_image = true;
            this_canvas.gameObject.SetActive(true);
        }

    }
    void UpdateUses()
    {
        uses1 = global_bb.GetValue<int>("Machine1Uses");
        uses2 = global_bb.GetValue<int>("Machine2Uses");
        uses3 = global_bb.GetValue<int>("Machine3Uses");
        uses4 = global_bb.GetValue<int>("Machine4Uses");
        uses5 = global_bb.GetValue<int>("Machine5Uses");
        uses6 = global_bb.GetValue<int>("Machine6Uses");
    }
   


}
