﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class Highscore : MonoBehaviour {

    private GameObject manager;
    private GetFinalValues val;
    public Text Days_text;
    public Text NVCC_text;
  //  public Text NVCI_text;
    public Text VIPCC_text;
  //  public Text VIPCI_text;

    void Start()
	{
        manager = GameObject.Find("Game Manager");
        val = manager.GetComponent<GetFinalValues>();
    }

	void Update()
	{
		//manager = GameObject.Find("Game Manager");
		//val = manager.GetComponent<GetFinalValues>();
		Days_text.text = val.d.ToString();
        NVCC_text.text = val.nvcc.ToString();
      //  NVCI_text.text = val.nvci.ToString();
        VIPCC_text.text = val.vipcc.ToString();
      //  VIPCI_text.text = val.vipci.ToString();

    }

	
}
