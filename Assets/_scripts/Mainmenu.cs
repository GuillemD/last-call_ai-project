﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mainmenu : MonoBehaviour {

	public void Playgame()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);

	}



	public void BackToMenu()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 1);
	}

	public void MenuFromWin(){
		
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 2);
	}

    public void MenuFromLose()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 3);
    }

	public void QuitGame ()
	{
		Debug.Log ("QUIT");
		Application.Quit();
	}
}
