﻿using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject spawnPrefab;
    private DayNightCycle dn;

   

    public float minSecondsBetweenSpawning = 5.0f;
    public float maxSecondsBetweenSpawning = 6.0f;

    private float savedTime;
    private float secondsBetweenSpawning;

    // Use this for initialization
    void Start () {
        dn = GetComponent<DayNightCycle>();
        savedTime = Time.time;
        secondsBetweenSpawning = Random.Range(minSecondsBetweenSpawning, maxSecondsBetweenSpawning);
        

    }
	
	// Update is called once per frame
	void Update () {

		if(GameManager.gameState == GameManager.gameStates.Day)
		{
            if(dn.days == 0)
            {
                minSecondsBetweenSpawning = 5f;
                maxSecondsBetweenSpawning = 6f;
            }
            if (dn.days == 2)
            {
                minSecondsBetweenSpawning = 4f;
                maxSecondsBetweenSpawning = 5f;
            }
            if (dn.days >= 3)
            {
                minSecondsBetweenSpawning = 2f;
                maxSecondsBetweenSpawning = 4f;
            }
            if (Time.time - savedTime >= secondsBetweenSpawning) // is it time to spawn again?
            {
            MakeThingToSpawn(spawnPrefab);
            savedTime = Time.time; // store for next spawn
            secondsBetweenSpawning = Random.Range(minSecondsBetweenSpawning, maxSecondsBetweenSpawning);
            }

		}
		if(GameManager.gameState == GameManager.gameStates.Night){
           
         }


			
    }

    public void MakeThingToSpawn(GameObject sp_prefab)
    {
        // create a new gameObject
        GameObject clone = Instantiate(sp_prefab, transform.position, transform.rotation) as GameObject;
    }
}
