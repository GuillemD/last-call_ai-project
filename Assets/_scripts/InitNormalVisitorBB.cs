﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class InitNormalVisitorBB : MonoBehaviour
{

    Blackboard bb;
    List<GameObject> control_list = new List<GameObject>();
    List<GameObject> litter_list = new List<GameObject>();
    // Use this for initialization
    void Start()
    {
        bb = GetComponent<Blackboard>();
        bb.SetValue("Exit", GameObject.Find("MainExit"));
        bb.SetValue("Plane", GameObject.Find("Plane"));
        bb.SetValue("qlast", GameObject.Find("Q_lastpos"));
        bb.SetValue("q9", GameObject.Find("Q_9"));
        bb.SetValue("q8", GameObject.Find("Q_8"));
        bb.SetValue("q7", GameObject.Find("Q_7"));
        bb.SetValue("q6", GameObject.Find("Q_6"));
        bb.SetValue("q5", GameObject.Find("Q_5"));
        bb.SetValue("q4", GameObject.Find("Q_4"));
        bb.SetValue("q3", GameObject.Find("Q_3"));
        bb.SetValue("q2", GameObject.Find("Q_2"));
        bb.SetValue("q1", GameObject.Find("Q_1"));
        bb.SetValue("Machine1", GameObject.Find("Machine1_pos"));
        bb.SetValue("Machine2", GameObject.Find("Machine2_pos"));
        bb.SetValue("Machine3", GameObject.Find("Machine3_pos"));
        bb.SetValue("Machine4", GameObject.Find("Machine4_pos"));
        bb.SetValue("Machine5", GameObject.Find("Machine5_pos"));
        bb.SetValue("Machine6", GameObject.Find("Machine6_pos"));

        control_list.Add(GameObject.Find("Control1_col"));
        control_list.Add(GameObject.Find("Control2_col"));
        control_list.Add(GameObject.Find("Control3_col"));

        bb.SetValue("controls", control_list);

        litter_list.Add(GameObject.Find("Litter1"));
        litter_list.Add(GameObject.Find("Litter2"));
        litter_list.Add(GameObject.Find("Litter3"));
        litter_list.Add(GameObject.Find("Litter4"));
        litter_list.Add(GameObject.Find("Litter5"));
        litter_list.Add(GameObject.Find("Litter6"));

        bb.SetValue("litter_positions", litter_list);
    }

    // Update is called once per frame
    void Update()
    {

    }
}