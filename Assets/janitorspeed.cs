﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class janitorspeed : MonoBehaviour {

	Blackboard bb;

	float speed;
	float increased_speed;

	public bool speed_up = false;


	void Start () 
	{
		bb = GetComponent<Blackboard>();
		speed = bb.GetValue<float>("Speed");
		increased_speed = speed * 3;
	}
	

	void Update () 
	{
		if(speed_up)
		{
			bb.SetValue("Speed", increased_speed);

		}
		else
		{
			bb.SetValue("Speed", speed);

		}
		
	}
		


	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("JanitorEmitter"))
		{
			speed_up = true;
		}
	}

	//private void OnTriggerExit(Collider other)
	//{
		//if (other.gameObject.CompareTag("JanitorEmitter"))
		//{
			//speed_up = false;
		//}
	//}
}
