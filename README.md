# README #

### AI Project ###

* Last Call is gonna be the project done in the subject of Artificial Intelligence.
* v0.4

Have you ever wanted to be in charge of the largest airport known to mankind? Now it's your chance!

From the first hours of the day work needs to be done everywhere. 
The first customers arrive and they need to be attended. The desks starts getting crowded and people dislike waiting too much... 
The next step is to go through the security control and, oh boy, hope you don't bring anything forbidden because the security guards won't have courtesy. 
Once they've gone through that they are finally ready to take the flight. Make sure they have something to eat, but be careful. 
There's rude people everywhere so be prepared for the littering. 
The waiting room is a heat zone, lots of people come at the same time and they are waiting for their flight. Don't lose it!
Then the night comes and the airport must be cleaned from top to bottom for the next day.
And the most important thing, make sure the passengers hear the LAST CALL, we don't want them to miss their flight!

### Innovation - Sound Perception ###

Agents react to Last Calls for flights as they finish what they are doing and go to the plane as fast as they can!

### How to Play? ###

Main menu: Here you can chose to play in fullscreen or windowed mode and the volume setting.
When you press play, the game starts.
You are in control of an airport so you need to organise what you buy first. People start entering the airport when the day comes but there is only one desk.
Each successful customer will pay for the ticket and then proceed to go to the queue to pass the security control.
After that, visitors will buy snacks at the vending machines if there is one available and litter on the floor.
As the owner you can buy: desks, an exclusive desk for VIPs that will increase their ticket price, vending machines, an exclusive control for VIPs that will increase their happiness and a boost for the janitor that will double his speed.
Your goal is to achieve 100 happiness and maintain it at least until the third day.
If you reach 0 happiness, you lose...
If there's a crowd in the waiting room you can use (once per day) the Last Call button. Everyone who hears it will go faster to the plane.

**To move the screen you can use your mouse or the WASD keys**


### Who do I talk to? ###

Owners of the project:

- Guillem Arman (guillemarman8@gmail.com)

- Guillem Dominguez (gdrdvilla@gmail.com)

- Link: https://bitbucket.org/GuillemD/last-call_ai-project

- Website: https://guillemd.github.io/LastCall-AI/

### License ###

MIT License

Copyright (c) 2017 Guillem Arman Janer and Guillem Dominguez Ruiz de Vila

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.